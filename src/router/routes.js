import {
  EVENTS,
  ELEMENT_TEMPLATES,
  FloatingGardensEngine,
} from "@floating-gardens/engine";
import { storeToRefs } from "pinia";
import { useLocalStorageDataBackend } from "src/data_storage/LocalStorageDataBackend";
import { engine, setEngine, setFrontEventListeners } from "src/engine";
import { useRoomStore } from "src/stores/rooms";
import { useUsersStore } from "src/stores/users";

const routes = [
  {
    path: "/login",
    name: "login",
    component: () => import("src/pages/LoginForm.vue"),
  },

  {
    path: "/solo",
    name: "soloplay",
    component: () => import("src/pages/IslandView.vue"),
    beforeEnter: async (to, from, next) => {
      setEngine(new FloatingGardensEngine(useLocalStorageDataBackend()));

      if (Object.keys(engine.users).length === 0) {
        await engine.createNewUser({
          login: "login",
          password: "password",
          pseudo: "New User",
        });
      }
      const userStore = useUsersStore();
      const { userId, users, party } = storeToRefs(userStore);
      const { rooms } = storeToRefs(useRoomStore());

      userId.value = Object.values(engine.users)[0]._id;
      users.value = { ...engine.users };
      party.value = Object.values(engine.parties)[0];
      rooms.value = { ...engine.rooms };

      await engine.startUpdating();
      next();
    },
  },

  {
    path: "/online",
    name: "onlineplay",
    component: () => import("src/pages/IslandView.vue"),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
