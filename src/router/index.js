import { storeToRefs } from "pinia";
import { route } from "quasar/wrappers";
import {
  addActionToHandle,
  engine,
  setEngineProxy,
  setFrontEventListeners,
} from "src/engine";
import { ACTIONS } from "@floating-gardens/engine";
import { useUsersStore } from "src/stores/users";
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";
import routes from "./routes";
import { useRoomStore } from "src/stores/rooms";

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === "history"
    ? createWebHistory
    : createWebHashHistory;

  const router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });

  router.beforeEach(async (to, from) => {
    if (to.name === "login") return true;
    if (to.name === "soloplay") return true;
    // New app load, fetch the one from the storage if there's one
    if (!engine || !engine.token) {
      console.log(
        "Didn't find any token in the store. Checking the local storage."
      );
      const storedToken = localStorage.getItem("token");
      // Uh oh, no token. Redirect to login.
      if (!storedToken) {
        console.log("Didn't find any token in the local storage...Login.");
        router.replace({ name: "login" });
        return;
      }

      // Ok, we've found a token but it's not in the store. Let's initiate a new connection and try to authenticate
      setEngineProxy(router);
      engine.authenticateWithToken(storedToken);
    }
    // We've found a token in the store, how is that possible? No idea how to handle that, AM I A SENTIENT MACHINE?
  });

  return router;
});
