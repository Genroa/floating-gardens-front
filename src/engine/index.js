import { ACTIONS, EVENTS } from "@floating-gardens/engine";
import { storeToRefs } from "pinia";
import { initialiseNewSocket, socket } from "src/lib/socket";
import { useRoomStore } from "src/stores/rooms";
import { useUsersStore } from "src/stores/users";

export let engine = null;
export function setEngine(e) {
  engine = e;
  setFrontEventListeners();
}
export function setEngineProxy(router) {
  const { userId, users, party: userParty } = storeToRefs(useUsersStore());
  const { rooms } = storeToRefs(useRoomStore());

  setEngine(new FloatingGardenEngineWebsocketProxy("http://localhost:3011"));
  engine.onError = () => {
    engine.stop();
    router.replace({ name: "login" });
  };
  engine.onSuccess = ({ user, party }) => {
    // TODO: THIS ENTIRE BIT SHOULD BE COMMON TO BOTH TYPES OF GAME.
    userId.value = user._id;
    users.value = { ...users.value, [user._id]: user };
    userParty.value = party;

    addActionToHandle({
      actionId: ACTIONS.QUERY_PARTY_USERS,
      cb: (usersOfParty) => {
        users.value = Object.fromEntries(usersOfParty.map((u) => [u._id, u]));
      },
    });
    addActionToHandle({
      actionId: ACTIONS.QUERY_PARTY_ROOMS,
      cb: (roomsOfParty) => {
        rooms.value = Object.fromEntries(roomsOfParty.map((i) => [i._id, i]));
      },
    });
    router.push({ name: "onlineplay" });
  };
}
// export function setLocalEngine() {}

export function setFrontEventListeners() {
  console.log("Set front event listeners");
  const { users } = storeToRefs(useUsersStore());
  const { rooms } = storeToRefs(useRoomStore());
  engine.setEventListener(EVENTS.UPDATED_ROOM, (room) => {
    // console.log("Room update:", room);
    rooms.value = { ...rooms.value, [room._id]: room };
  });

  engine.setEventListener(EVENTS.UPDATED_USER, (user) => {
    users.value = { ...users.value, [user._id]: user };
  });

  engine.setEventListener(EVENTS.USER_MOVED, ({ user }) => {
    users.value = { ...users.value, [user._id]: user };
  });
}

export function addActionToHandle({ actionId, data, cb }) {
  if (engine instanceof FloatingGardenEngineWebsocketProxy) {
    engine.addActionToHandle({ actionId, data, cb });
  } else {
    const { user, party } = useUsersStore();
    engine.addActionToHandle({ actionId, context: { user, party }, data, cb });
  }
}

export class FloatingGardenEngineWebsocketProxy {
  parties = {};
  users = {};
  rooms = {};
  listeners = {};
  actionsPile = [];
  url = null;
  onError = null;
  onSuccess = null;
  token = null;

  // url: "http://localhost:3011"
  constructor(url, onError, onSuccess) {
    this.url = url;
    this.onError = onError ?? (() => {});
    this.onSuccess = onSuccess ?? (() => {});
  }

  stop() {
    socket.disconnect();
  }

  assignNewToken(renewedInformations) {
    if (!renewedInformations) {
      this.token = null;
      console.log("Something's wrong. Expired token or bad user/mdp.");
      this.onError();
      return false;
    }
    const { token, user, party } = renewedInformations;
    this.token = token;
    this.users = { ...this.users, [user._id]: user };
    this.party = party;
    localStorage.setItem("token", token);
    return true;
  }

  setSocketListeners() {
    console.log("Setting up WS listeners for events:", Object.keys(EVENTS));
    for (const eventId of Object.keys(EVENTS)) {
      socket.on(`EVENT:${eventId}`, (data) => {
        // console.log(`Received event type "${eventId}":`, data);
        this.callEventListener(eventId, data);
      });
    }
  }

  authenticateWithToken(token) {
    console.log("Authenticating using the existing token", token);
    initialiseNewSocket(this.url);
    socket.on("connect", () => {
      socket.emit("authenticate_token", token, (renewedInformations) => {
        if (this.assignNewToken(renewedInformations)) {
          this.setSocketListeners();
          this.onSuccess(renewedInformations);
        }
      });
    });
  }

  authenticate(login, password) {
    initialiseNewSocket(this.url);
    socket.on("connect", () => {
      socket.emit("authenticate", login, password, (renewedInformations) => {
        if (this.assignNewToken(renewedInformations)) {
          this.setSocketListeners();
          this.onSuccess(renewedInformations);
        }
      });
    });
  }

  callEventListener(eventId, data) {
    if (!this.listeners[eventId]) return;
    else this.listeners[eventId](data);
  }

  setEventListener(eventId, cb) {
    if (!EVENTS[eventId])
      throw new Error(`Event ID "${eventId}" doesn't exist`);
    this.listeners[eventId] = cb;
  }

  addActionToHandle({ actionId, data, cb }) {
    socket.emit(`ACTION:${actionId}`, data, cb);
  }

  async loadData() {}

  async createNewUser({ login, password, pseudo }) {
    // const { user, party, room } = await this.dataBackendAdapter.createNewUser(
    //   {
    //     login,
    //     password,
    //     pseudo,
    //   }
    // );
    // this.users[user._id] = user;
    // this.parties[party._id] = party;
    // this.rooms[room._id] = room;
    // return { user, party, room };
  }

  async createNewUserInParty({ login, password, pseudo, partyId }) {
    // return await this.dataBackendAdapter.createNewUser({
    //   login,
    //   password,
    //   pseudo,
    //   partyId,
    // });
  }
}
