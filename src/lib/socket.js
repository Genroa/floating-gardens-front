import { io } from "socket.io-client";

export let socket = null;

export function initialiseNewSocket(url) {
  if (socket) socket.disconnect();
  socket = io(url);
}
