export const useLocalStorageDataBackend = () => ({
  async loadData() {
    return {
      users: JSON.parse(localStorage.getItem("users")) || {},
      parties: JSON.parse(localStorage.getItem("parties")) || {},
      rooms: JSON.parse(localStorage.getItem("rooms")) || {},
    };
  },
  saveAll({ users, parties, rooms }) {
    console.log("SaveAll called!");
    localStorage.setItem("users", JSON.stringify(users));
    localStorage.setItem("parties", JSON.stringify(parties));
    localStorage.setItem("rooms", JSON.stringify(rooms));
  },
  // This version doesn't even expect a partyId because in solo play you can't join an existing party
  async createNewUser({ login, password, pseudo }) {
    return {
      user: {
        _id: "0",
        login,
        password,
        pseudo,
        partyId: "0",
        visitingRoom: "0",
      },
      party: {
        _id: "0",
        rooms: ["1"],
        inventory: "0",
      },
      room: {
        _id: "1",
        name: "New Room",
        elements: [],
        contextProperties: {},
      },
      inventory: {
        _id: "0",
        name: "Inventory",
        elements: [],
        contextProperties: {},
      },
    };
  },
});
