import { defineStore, storeToRefs } from "pinia";
import { v4 as uuid } from "uuid";
import { useUsersStore } from "./users";

export const useRoomStore = defineStore("rooms", {
  state: () => ({
    rooms: {},
  }),
  getters: {
    visitingRoom() {
      const { user } = storeToRefs(useUsersStore());
      return user.value && this.rooms[user.value.visitingRoom];
    },
  },
  actions: {},
});
