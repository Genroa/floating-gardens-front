import { defineStore } from "pinia";
import { engine } from "src/engine";
// import {
//   initialiseNewSocket,
//   setSocketListeners,
//   socket,
// } from "src/lib/socket";

export const useUsersStore = defineStore("users", {
  state: () => ({
    userId: null,
    users: {},
    party: null,
  }),

  getters: {
    user() {
      return this.userId ? this.users[this.userId] : null;
    },
    otherUsers() {
      return Object.values(this.users).filter((u) => u._id !== this.userId);
    },
    presentOtherUsers() {
      if (!this.user) return [];
      return Object.values(this.otherUsers).filter(
        (u) => u.visitingRoom === this.user.visitingRoom
      );
    },
  },

  actions: {
    logOut() {
      localStorage.removeItem("token");
      this.$reset();
      engine.stop();
      this.router.push({ name: "login" });
    },
  },
});
